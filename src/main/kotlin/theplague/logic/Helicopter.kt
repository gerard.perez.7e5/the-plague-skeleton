package theplague.logic

import theplague.interfaces.Position
import kotlin.math.abs

class Helicopter(): Vehicle() {
    override fun canMove(curentPosition: Position, newPosition: Position): Boolean = true
    override val icon = "\uD83D\uDE81"
}