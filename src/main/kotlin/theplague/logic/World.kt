package theplague.logic


import theplague.interfaces.IWorld
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import kotlin.random.Random


class World(
    var zombieColdown: Int = 0,
    override val width: Int = 10,
    override val height: Int = 10,
    override val territories: List<List<Territory>> = MutableList(height) { MutableList(width) { Territory() } },
    override val player: Player = Player()
) : IWorld {

    init {
        val territory = territories[4][5]
        territory.player = player
        player.position = Position(5, 4)
    }


    override fun nextTurn() {
        generateNewItems()
        genereteNewColonies()
        player.nextTurn()
    }

    override fun gameFinished(): Boolean = false

    override fun canMoveTo(position: Position): Boolean {
        return player.canMoveTo(position)
    }
    // Se puede mover utilizar la function canMove() vehicular
    // Comprobar que la position en la que el jugador intenta moverse es accesible por el vehículo que estamos utilizando
    // Ej: Los pies solo 1 alrededor, La bicicleta 4 casillas, y el helicopter donde quieras

    override fun moveTo(position: Position) {
        // eliminar jugador del terriotri actual
        val currentTerritory = playerposition
        currentTerritory.player = null

        // posar jugador a terrirori a la "position"
        player.position = position
        // actualizar posicio jugador
        territories[position.y][position.x].player = player

    }

    override fun exterminate() {

    }

    override fun takeableItem(): Iconizable? {
        return if (playerposition.item != null) {
            playerposition.item
        } else null
    }


    val playerposition get() = territories[player.position.y][player.position.x]
    override fun takeItem() {
        player.equip(playerposition.item!!)
        playerposition.item = null
    }

    fun generateNewItems() {
        val probability = Random.nextInt(0, 100)
        val territori = territories[Random.nextInt(0, 10)][Random.nextInt(0, 10)]
        when (probability) {
            in 31..55 -> territori.item = Scooter()
            in 51..65 -> territori.item = Helicopter()
            in 66..90 -> territori.item = Stick()
            in 91..100 -> territori.item = Broom()
        }
    }

    fun genereteNewColonies() {
        if (zombieColdown != 0) {
            zombieColdown--
        }
        val newColony = choseAColony()
        val territori = territories[Random.nextInt(0, 10)][Random.nextInt(0, 10)]
        territori.colony = newColony
    }

    fun choseAColony(): Colony? {
        return when (Random.nextInt(0, 100)) {
            in 0..30 -> null
            in 31..70 -> Majora()
            in 71..100 -> if (zombieColdown == 0) {
                Zombie()
            } else choseAColony()
            else -> null
        }
    }
}