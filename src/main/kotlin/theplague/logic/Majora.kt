package theplague.logic

import theplague.interfaces.Position
import kotlin.random.Random

class Majora() : Colony() {
    var reproduceCooldown = 0
    override val levelOfDanger = 1
    override val icon = "\uD83E\uDD21"
    override fun willReproduce(): Boolean {
        return if (reproduceCooldown == 0) {
            reproduceCooldown = 5
            true
        } else {
            reproduceCooldown--
            false
        }
    }

    override fun reproduce() {

    }



    override fun atacked(weapon: Weapon) {
        if (weapon == Stick()) {
            size -= 2
        } else size--
    }
}