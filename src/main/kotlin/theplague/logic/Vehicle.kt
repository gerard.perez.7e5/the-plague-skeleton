package theplague.logic

import theplague.interfaces.Iconizable
import theplague.interfaces.Position

abstract class Vehicle  : Item() {
    abstract fun canMove(curentPosition: Position, newPosition: Position): Boolean
}