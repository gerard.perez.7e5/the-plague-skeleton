package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable


class Territory(var player: Player? =null, var item: Item?=null, var colony: Colony?=null) : ITerritory {
    override fun iconList(): List<Iconizable> {
        return listOfNotNull(player, item, colony)
    }
}