package theplague.logic

class Zombie : Colony() {
    var reproduceCooldown = 0
    override val levelOfDanger = 2
    override val icon = "\uD83E\uDDDF"
    override fun willReproduce(): Boolean{
        return if (reproduceCooldown == 0){
            reproduceCooldown=5
            true
        } else {
            reproduceCooldown--
            false
        }
    }
    override fun reproduce() {
        size++
    }



    override fun atacked(weapon: Weapon) {
        if (weapon == Broom()){
            size-=2
        } else size--
    }
}