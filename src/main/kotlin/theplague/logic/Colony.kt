package theplague.logic

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import kotlin.random.Random


abstract class Colony : Iconizable {
    var size: Int = 1
    abstract val levelOfDanger: Int
    abstract fun willReproduce(): Boolean
    abstract fun reproduce()
    fun needsToExpand(): Boolean {
        return size > 2
    }
    abstract fun atacked(weapon: Weapon)
    fun colonizedBy(plage: Colony): Colony {
        return if (plage.levelOfDanger > levelOfDanger) {
            plage
        } else if (plage.levelOfDanger < this.levelOfDanger) {
            this
        } else {
            when (Random.nextInt(0, 1)) {
                0 -> plage
                1 -> this
                else -> plage
            }
        }
    }
}