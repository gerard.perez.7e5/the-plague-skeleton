package theplague.logic

import theplague.interfaces.Position
import kotlin.math.abs

class Scooter(): Vehicle() {
    override fun canMove(curentPosition: Position, newPosition: Position): Boolean {
        val xDist = abs(curentPosition.x-newPosition.x)
        val yDist = abs(curentPosition.y-newPosition.y)

        return if (xDist>=1 && yDist<=0 || xDist<=0 && yDist>=1 || xDist==0 && yDist==0){
            true
        }else
            false
    }
    override val icon = "\uD83D\uDEF4"
}