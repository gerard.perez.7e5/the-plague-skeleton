package theplague.logic

import theplague.interfaces.Position
import kotlin.math.abs

class OnFoot(): Vehicle() {
    override fun canMove(curentPosition: Position, newPosition: Position): Boolean {
        val xDist = abs(curentPosition.x-newPosition.x)
        val yDist = abs(curentPosition.y-newPosition.y)

        return if (xDist<=1 && yDist<=1){
            true
        }else
            false




    }
    override val icon = "\uD83D\uDEB6"
}