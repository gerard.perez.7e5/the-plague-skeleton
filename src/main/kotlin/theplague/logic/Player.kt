package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position


class Player(

    override var turns: Int = 0,
    override var livesLeft: Int = 20,
    override var currentWeapon: Weapon = Hand(),
    override var currentVehicle: Vehicle = OnFoot(),
    var position: Position = Position(0, 0)


) : IPlayer, Iconizable {
    override val icon = "\uD83D\uDEB6"
    fun equip(item: Item) {
        when(item){
            is Stick -> currentWeapon=item
            is Broom -> currentWeapon= item
            is Scooter -> currentVehicle= item
            is Helicopter -> currentVehicle= item
        }
        }

    fun canMoveTo(newPosition: Position): Boolean {
        return currentVehicle.canMove(position, newPosition)
    }

    fun nextTurn() {
        turns++
    }
}
